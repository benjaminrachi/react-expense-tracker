import React, { Fragment, useEffect } from 'react';
import { connect } from 'react-redux';
import ExpenseForm from "../components/ExpenseForm";
import { editExpense } from '../actions/expenses'

const EditExpensePage = (props) => {
    useEffect(() => {
        console.log('EditExpensePage', props)
    })
    const editExpenseHandler = (form) => {
        props.dispatch(editExpense(props.match.params.id, form));
        props.history.push('/')
    }
    return (
        <Fragment>
            <h2 className="text-center m-4">Edit expense</h2>
            <ExpenseForm onSubmit={editExpenseHandler} expense={{ ...props.expense }} />
        </Fragment>
    );
}

const mapStateToProps = (state, props) => {
    return {
        expense: state.expenses.find(exp => exp.id === props.match.params.id)
    }
}

export default connect(mapStateToProps)(EditExpensePage);