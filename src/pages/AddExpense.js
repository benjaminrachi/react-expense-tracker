import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import ExpenseForm from "../components/ExpenseForm";
import { addExpense } from '../actions/expenses'

const AddExpensePage = (props) => {
    const addExpenseHandler = (form) => {
        props.dispatch(addExpense(form));
        props.history.push('/')
    }
    return (
        <Fragment>
            <h2 className="text-center m-4">Create a new expense</h2>
            <ExpenseForm onSubmit={addExpenseHandler} />
        </Fragment>
    );
}

export default connect()(AddExpensePage);