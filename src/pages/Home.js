import React, { Fragment } from 'react';
import ExpenseDashboard from '../components/ExpenseDashboard'

const HomePage = () => {
    return (
        <Fragment>
            <ExpenseDashboard />
        </Fragment>
    );
}

export default HomePage;