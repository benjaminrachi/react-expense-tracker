import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import HomePage from '../pages/Home'
import AddExpensePage from '../pages/AddExpense'
import EditExpensePage from '../pages/EditExpense'
import HelpPage from '../pages/Help'
import NotFoundPage from '../pages/NotFound'
import Header from '../components/Header';

const AppRouter = () => {
    return (
        <BrowserRouter>
            <div>
                <Header />
                <div className="container mt-4">
                    <Switch>
                        <Route path="/" component={HomePage} exact={true} />
                        <Route path="/create" component={AddExpensePage} />
                        <Route path="/edit/:id" component={EditExpensePage} />
                        <Route path="/help" component={HelpPage} />
                        <Route component={NotFoundPage} />
                    </Switch>
                </div>
            </div>
        </BrowserRouter>
    );
}

export default AppRouter;