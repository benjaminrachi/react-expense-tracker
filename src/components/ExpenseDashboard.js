import React, { Fragment } from 'react';
import ExpenseList from './ExpenseList';

const ExpenseDashboard = () => {
    return (
        <Fragment>
            <h2 className="my-3">Expenses dashboard</h2>
            <ExpenseList />
        </Fragment>
    );
}

export default ExpenseDashboard;