import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import ExpenseItem from './ExpenseItem'

const getExpensesItems = (expenses) => {
    return expenses.length
        ? expenses.map(expence => {
            return <ExpenseItem key={expence.id} {...expence} />
        })
        : <p>No expenses to show yet</p>
}

const ExpenseList = (props) => {
    return (
        <Fragment>
            {getExpensesItems(props.expenses)}
        </Fragment>
    );
};



const mapStateToProps = (state) => {
    return {
        expenses: state.expenses
    }
}
export default connect(mapStateToProps)(ExpenseList);