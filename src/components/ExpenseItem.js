import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';



const ExpenseItem = (props) => {
    const date = moment(props.expenseDate).format("MM-DD-YYYY");
    return (
        <div className="card expense-item-card">
            <div className="card-body">
                <h5 className="card-title">{props.description}</h5>
                <p className="card-subtitle">{date}</p>
                <p className="card-text">$ {props.amount}</p>
                <Link to={`/edit/${props.id}`} className="card-link">See more</Link>
            </div>
        </div>
    );
}

export default ExpenseItem;