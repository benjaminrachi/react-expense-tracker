import React, { Component } from 'react';
import moment from 'moment';
import 'react-dates/initialize';
import { SingleDatePicker } from 'react-dates';
import { connect } from 'react-redux';
import { compose } from 'redux'
import { withRouter } from 'react-router-dom';
import { removeExpense } from '../actions/expenses'

class ExpenseForm extends Component {
    constructor(props) {
        super(props);

        if (props.expense) {
            this.state = this.initStateWithValue(props.expense)
        }
        else {
            this.state = this.initStateWithDefault();
        }
    }

    initStateWithValue(expense) {
        return {
            ...expense,
            expenseDate: moment(expense.expenseDate)
        }
    }
    initStateWithDefault() {
        return {
            description: '',
            note: '',
            amount: '',
            expenseDate: moment(),
            calendarFocused: false,
            error: '',
        };
    }


    onDescriptionChange = (e) => {
        const description = e.target.value;
        this.setState(() => ({ description }));
    };
    onNoteChange = (e) => {
        const note = e.target.value;
        this.setState(() => ({ note }));
    };
    onAmountChange = (e) => {
        const amount = e.target.value;

        if (amount.match(/^\d*(\.\d{0,2})?$/)) {
            this.setState(() => ({ amount }));
        }
    };
    onDateChange = (expenseDate) => {
        this.setState(() => ({ expenseDate }));
    };
    onFocusChange = ({ focused }) => {
        this.setState(() => ({ calendarFocused: focused }));
    };
    onSubmit = (e) => {
        e.preventDefault();
        if (!this.isFormValid()) {
            this.setState(() => ({ error: 'Please fill all the required information' }))
            return;
        }
        else {
            this.props.onSubmit({
                description: this.state.description,
                amount: parseFloat(this.state.amount, 10),
                expenseDate: this.state.expenseDate.valueOf(),
                note: this.state.note
            })
        }
    };
    isFormValid = () => {
        return this.state.description && this.state.amount;
    };
    onRemove = () => {
        this.props.dispatch(removeExpense({ id: this.props.expense.id }));
        this.props.history.replace('/')
    };
    render() {
        return (
            <form className="expense-form" onSubmit={this.onSubmit}>
                {this.state.error &&
                    <div className="alert alert-danger" role="alert">
                        {this.state.error}
                    </div>
                }
                <div className="form-group">
                    <label>Expense description</label>
                    <input
                        type="text"
                        className="form-control"
                        name="description"
                        placeholder="Expense description"
                        autoFocus
                        required
                        value={this.state.description}
                        onChange={this.onDescriptionChange}
                    />
                </div>
                <div className="form-group">
                    <label>Expense amount</label>
                    <input
                        type="text"
                        className="form-control"
                        name="amount"
                        placeholder="Expense amount"
                        required
                        value={this.state.amount}
                        onChange={this.onAmountChange}
                    />
                </div>
                <div className="d-flex justify-content-between">
                    <label className="date-label">Expense date</label>
                    <SingleDatePicker
                        placeholder="Expense date"
                        date={this.state.expenseDate}
                        onDateChange={this.onDateChange}
                        focused={this.state.calendarFocused}
                        onFocusChange={this.onFocusChange}
                        showClearDate={true}
                        numberOfMonths={1}
                        isOutsideRange={() => false}
                    />
                </div>
                <div className="form-group">
                    <label>Note</label>
                    <textarea
                        className="form-control"
                        name="note"
                        rows="5"
                        placeholder="Add a note for your expense (optional)"
                        value={this.state.note}
                        onChange={this.onNoteChange}
                    >
                    </textarea>
                </div>
                {this.props.expense && this.props.expense.id &&
                    <button onClick={this.onRemove} className="btn btn-outline-danger float-left">DELETE</button>
                }
                <button type="submit" className="btn btn-outline-primary float-right">SAVE</button>
            </form>
        );
    }
}

export default compose(
    withRouter,
    connect()
)(ExpenseForm);