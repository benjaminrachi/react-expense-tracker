import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = () => {
    return (
        <nav className="navbar navbar-expand navbar-dark bg-dark">
            <NavLink to="/" activeClassName="is-active" exact={true}>
                <span className="navbar-brand mb-0 h1">Expenses tracker app</span>
            </NavLink>
            <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                    <NavLink className="nav-link" to="/create" activeClassName="active">Create</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/help" activeClassName="active">Help</NavLink>
                </li>
            </ul>
        </nav>
    );
}

export default Header;